import React , { Component } from 'react';
import * as metadata from '../constants/metadata';

class StateText extends Component {

	constructor(props) {
		super(props)
		this.state = {adviceType: -1};
	}

    componentWillMount() {
        if (this.props.realPrice) {
            this.buyAdvice();
        }
    }

    componentDidUpdate(prevProps, prevState) {
        if (this.props.realPrice !== prevProps.realPrice) {
            this.buyAdvice();
        }
    }

    buyAdvice() {
        let type = 0;
        let realPrice = this.props.realPrice;
        let minPrice = this.props.minPrice;
        let maxPrice = this.props.maxPrice;
        if(realPrice < minPrice) {
            type = 1;
        } else if(realPrice > maxPrice) {
            type = 2;
        }
        this.setState({adviceType: type});	
    }

	render() {
        let adviceText = "";
        if(this.state.adviceType > -1) {
            adviceText = metadata.PurchaseAdvice[this.state.adviceType];
        }
		return (
			<span> {adviceText} </span>
		);			
	}
}

export default StateText;
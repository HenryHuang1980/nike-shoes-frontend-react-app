import React, { Component } from 'react';
import * as metadata from '../constants/metadata';
import StateText from './StateText';

class ShoeList extends Component {

	constructor(props) {
		super(props);
		this.state = {shoeItems: []};
	}

    componentWillMount() {
        if (this.props.content && this.props.content.length) {
            this.setState({ shoeItems: this.props.content });
        }
    }

    componentDidUpdate(prevProps, prevState) {
        if (this.props.content !== prevProps.content) {
            this.setState({ shoeItems: this.props.content });
        }
    }

    render() {
		let header = metadata.ShoeListHeader; 
        let headerClass = "Header-Item";
		let headerNodes = header.map((h, index) => {
			return (<th key={index} className={headerClass} >{h}</th>);
		});

        let bodyNodes = this.state.shoeItems.map((r, index) => {
			return (
                <tr key={r.shoeId}>
                    <td>{r.model}</td>
                    <td>{r.originalPrice}</td>
                    <td>{r.discountedPrice}</td>
                    <td>
                        <StateText realPrice={r.discountedPrice} minPrice={r.minPrice} maxPrice={r.maxPrice} />
                    </td>                                     
                </tr>
            );
		});

		return (
			<div>
				<table className="List-All">
	            	<thead>
	                    <tr key={0}>
	                       {headerNodes}
	                    </tr>
	                </thead>
	                <tbody>
	                	{bodyNodes}
	                </tbody>
	            </table >
	        </div>
		);			
	}
}

export default ShoeList;         
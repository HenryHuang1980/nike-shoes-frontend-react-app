import * as metadata from '../constants/metadata';

class ShoeDataLoader {

    constructor() {
        this.httpHeaders = {
            "Content-Type": "application/json;charset=UTF-8"
        };
    }

    obtainPriceById(shoeId) {
        let that = this;
        let pm = new Promise((resolve, reject) => {
            debugger;
            fetch(metadata.BackendBaseURI + "/shoe-price/" + shoeId, {
                method: "GET",
                mode: "cors",
                headers: that.httpHeaders
            }).then((response) => response.json())
                .then((responseData) => {
                    resolve(responseData);
                })
                .catch((error) => {
                    reject(error);
                });
        });
        return pm;           
    }
}

const shoeDataLoader = new ShoeDataLoader();
export default shoeDataLoader;
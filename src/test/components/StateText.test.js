import { render, screen } from '@testing-library/react';
import StateText from '../../components/StateText';

describe('<StateText />', () => {
    test('shows Moderate state, can buy now!', () => {
        const discountedPrice = 36, minPrice = 20, maxPrice = 50;
        render(<StateText realPrice={discountedPrice} minPrice={minPrice} maxPrice={maxPrice} />);
        const stateTextElt = screen.getByText(/Moderate state, can buy now!/i);
        expect(stateTextElt).toBeInTheDocument();
    });

    test('shows Best time to buy!', () => {
        const discountedPrice = 15.5, minPrice = 35.5, maxPrice = 65.5;
        render(<StateText realPrice={discountedPrice} minPrice={minPrice} maxPrice={maxPrice} />);
        const stateTextElt = screen.getByText(/Best time to buy!/i);
        expect(stateTextElt).toBeInTheDocument();
    });

    test('shows Can wait for discount', () => {
        const discountedPrice = 75.2, minPrice = 24.8, maxPrice = 52.5;
        render(<StateText realPrice={discountedPrice} minPrice={minPrice} maxPrice={maxPrice} />);
        const stateTextElt = screen.getByText(/Can wait for discount/i);
        expect(stateTextElt).toBeInTheDocument();
    });       
    
    test('shows empty advice', () => {
        render(<StateText />);
        const stateTextElt = screen.queryByText(/Can wait for discount/i);
        expect(stateTextElt).toBeNull();
    });      
});    
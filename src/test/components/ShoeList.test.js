import { render, screen } from '@testing-library/react';
import ShoeList from '../../components/ShoeList';

describe('<ShoeList />', () => {
    test('shows empty list', () => {
        render(<ShoeList />);
        const shoeListElt = screen.queryByText(/Can wait for discount/i);
        expect(shoeListElt ).toBeNull();
    }); 
    
    test('shows Can wait for discount', () => {
        const data = [
            {
                shoeId: '5',
                model: 'Nike Renew Ride 3',
                minPrice: 180,
                maxPrice: 200,
                originalPrice: 50,
                discountedPrice: 30
            },
            {
                shoeId: '6',
                model: 'Nike Air Max 90',
                minPrice: 120,
                maxPrice: 150,
                originalPrice: 210,
                discountedPrice: 126               
            }
        ];
        render(<ShoeList content={data} />);
        const stateElt = screen.getByText(/Best time to buy!/i);
        expect(stateElt).toBeInTheDocument();
    });   
});       
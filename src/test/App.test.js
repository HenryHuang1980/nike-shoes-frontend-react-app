import { render, screen, fireEvent } from '@testing-library/react';
import App from '../App';

describe('<App />', () => {
  test('renders Shoes List', () => {
    render(<App />);
    const shoeListElt = screen.getByText(/Shoes List/i);
    expect(shoeListElt).toBeInTheDocument();
  });

  test('refresh the data', () => {
    render(<App />);
    const refreshButton = screen.getByText("Refresh Data");
    fireEvent.click(refreshButton);
    const tableElt = screen.getByRole("table");
    expect(tableElt).toBeInTheDocument();
  });  
});
export const BackendBaseURI = "http://localhost:8081/api";
export const ShoeListHeader = ['Model', 'Original Price', 'Discounted Price', 'State']; 
export const ShoeDatabase = [
    {
      shoeId: '1',
      model: 'Nike Air Max 95 SE',
      minPrice: 120,
      maxPrice: 150,
    },
    {
      shoeId: '2',
      model: 'Nike Air Max 97 SE',
      minPrice: 5,
      maxPrice: 150,
    },
    {
      shoeId: '3',
      model: 'Nike Air Max Pre-Day',
      minPrice: 120,
      maxPrice: 160,
    },
    {
      shoeId: '4',
      model: 'Nike Air Max 270',
      minPrice: 100,
      maxPrice: 130,
    },
    {
      shoeId: '5',
      model: 'Nike Renew Ride 3',
      minPrice: 180,
      maxPrice: 200,
    },
    {
      shoeId: '6',
      model: 'Nike Air Max 90',
      minPrice: 120,
      maxPrice: 150,
    }
  ];
export const PurchaseAdvice = [
    "Moderate state, can buy now!", "Best time to buy!", "Can wait for discount"
];
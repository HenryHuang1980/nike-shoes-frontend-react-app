
import React, { Component } from 'react';
import './App.css';
import * as metadata from './constants/metadata';
import ShoeList from './components/ShoeList';
import shoeDataLoader from './services/ShoeDataLoader';

class App extends Component {
	constructor(props) {
		super(props);	    
		this.state = {shoeList: null, errMessage: ""};     
	}

    componentWillMount() {
        this.retrieveData();
    }

	retrieveData() {
        let that = this;
		let shoeList = [];
        Object.assign(shoeList, metadata.ShoeDatabase);
		let requests = [];
        shoeList.forEach((item, index)=>{
            requests[index] = shoeDataLoader.obtainPriceById(item.shoeId);
        });
		Promise.all(requests).then(values => {
            debugger;
            let errMessage = null;
			values.forEach((vItem, idx)=>{
                if(vItem && vItem.statusCode === 0) {
                    shoeList[idx].originalPrice = vItem.data.originalPrice;
                    shoeList[idx].discountedPrice = vItem.data.discountedPrice;
                } else {
                    let errMsg = "Invalid response";
                    if(vItem && vItem.errorMsg) {
                        errMsg = vItem.errorMsg;
                    }
                    if(errMsg.length > 0) {
                        errMessage = errMsg;
                    }
                    console.log(errMsg);
                }
            });
            if(errMessage) {
                that.setState({errMessage: errMessage});
            } else {
                that.setState({shoeList: shoeList});
            }            
		})
        .catch((error) => {
            that.setState({errMessage: JSON.stringify(error)});
        });
	}

    render() { 
		return (
			<div className="Content-Wrapper">	
                <h2>Shoes List</h2>
	            <ShoeList content={this.state.shoeList} />
                <button type="button" className="Refresh-Btn" onClick={this.retrieveData.bind(this)}> Refresh Data </button>
                <div className="Error-Info">{this.state.errMessage}</div>
    		</div>
        );
	}        
}

export default App;        